# Review Notes

by Christian Jensen
9/30/20

Thanks for doing the React certification!

I've given feedback throughout the codebase, and you can view all my changes easily with Git. Most of this feedback is just for your information to help strengthen your React skills, but some involve required changes. I have marked required changes with `TODO`, so you can easily do a global search for `TODO` and find all the required changes.

*NOTE:* There are some places where I've put a TODO that reads, "Pass a function into `setState` when relying on current state". To explain - you cannot rely on simply reading the `state` object to get the current state, since React updates state asynchronously. That is why React allows you to pass a function into `setState` that will provide the current state. Do this any time you need to set a new state that is based on the current state.

Once you've updated the places where a required change is listed, let me know and I can review them.

If you have any questions, please don't hesitate to reach out!

## General Feedback (changes listed below are not required)

- Good job with folder/file organization!
- I see many instances of inconsistent variable names. For example, `showPasswordbuttonIsDisabled`. The "b" in "button" should be capitalized to keep consistency with camel-cased variable names. Or the "Jokes" key in your Redux store, which should be all lowercase like the others. Or "mymodal" as a class name, which should be kebab-cased ("my-modal"). Keep things consistent and in step with common conventions to make it easier on other devs and your future self. JS variables are *camelCased*, while HTML tag names and CSS class names are *kebab-cased*.
- I see several instances of updating state multiple times within the same function. React will batch these to optimize, but I recommend getting your changes ready first, then calling `setState` only once. It will be easier to read for other devs and your future self.
- There are various places where you declare a `temp` variable. Often it for the sake of holding a value that you want to pass into a function such as `setState`. This is unnecessary clutter for your code, however, and I would recommend not using any type of `temp` variable pretty much ever. I have not seen a scenario where a temp variable was really needed. Instead, just pass the value directly. In other words, instead of this:

  ```javascript
  const tempFruit = ['apples', 'oranges']
  this.setState({ fruit: tempFruit })
  ```

  Do this:

  ```javascript
  this.setState({ fruit: ['apples', 'oranges'] })
  ```

- Put forms inside a `form` element. This will keep your HTML semantic, and enables form submission by simply hitting the enter key. You could use this on the login page and the search page.
  - You should also make sure the non-submit buttons inside the form have `type="button"` as `type="submit"` is the default.
