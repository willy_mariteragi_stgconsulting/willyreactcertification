Testing code coverage location: 
...\willyreactcertification\willy-react-certification\coverage\lcov-report\index.html

/*
    #Array Function - Filter
    ** filter function will help to remove or isolate items from a list
    ** in this example below, all object will be return except for the object where id==1
*/
  it("Array Function - Filter", () => {
    let listOfObject = [
      { id: 1, name: "item1", item1: true },
      { id: 2, name: "item2", item1: false },
      { id: 3, name: "item3", item1: false },
      { id: 4, name: "item4", item1: true },
    ];
    const result = listOfObject.filter((temp) => temp.id !== 1);

    expect(result.length).toBe(3);
  });

  
 /*
    #Array Function - Reduce
    ** Reduce will return 1 item. everything will depend on how the reducer is implemented
    ** in this example, my reducer is multiplying each item to the next on. or 3 factorial
*/
  it("Array Function - Reduce", () => {
    const array1 = [1, 2, 3];
    const myReducer = (accumulator, currentValue) => accumulator * currentValue;
    const result = array1.reduce(myReducer);

    expect(result).toBe(6);
  });
});


 
 import React, { useEffect,useReducer, useDebugValue, createContext } from "react";
/* 
#Choose 4 unused Hooks
    * useEffect
    *** usage: the use effect get automtically trigger when it the component is loading and when there is a change happening
    
    * useReducer
    *** usage: the reducer function dictate the permutation. the useReduce gives the current state and the way to update the state.
    
    * useDebugValue
    *** this will add what ever we passed on to the devtools under the name of the function component we used it.

    * createContext
    *** Create a context value that can be use where we only need to update in one place. First, we setup the value using createContext. Then we call useContext with the created context. then we use the value store within the context
 */

const themes = {
    redBackGround: {
      color: white,
      background: red,
    },
    blueBackGround: {
      color: yelllow,
      background: blue,
    },
  };
  const ThemeContext = createContext(themes.light);

  function example() {
  const [state, dispatch] = useReducer(reducer, initialState);

  useDebugValue(date, (date) => date.toDateString());

  
  useEffect(() => {
    console.log(`useEffect called Clicked state.count: ${state.count}`);
  });
 const context = useContext(ThemeContext)
  return (
    <div style={{color:context.color, background=context.background}}>
      <p>count: {state.count}</p>
      <button onClick={() => dispatch({ type: "decrement" })}>-</button>
      <button onClick={() => dispatch({ type: "increment" })}>+</button>
    </div>
  );
}
const initialState = { count: 0 };

function reducer(state, action) {
  switch (action.type) {
    case "increment":
      return { count: state.count + 1 };
    case "decrement":
      return { count: state.count - 1 };
    default:
      throw new Error();
  }
}
it("", () => {});
