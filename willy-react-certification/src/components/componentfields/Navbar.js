import React, { Component } from "react";
import { Link } from "react-router-dom";

class NavBar extends Component {
  render() {
    const navStyle = {
      color: "white",
    };

    return (
      <nav>
        <h3>Logo</h3>
        <ul className="nav-links">
          <li>
            <Link style={navStyle} to="/">
              Home
            </Link>
          </li>
          
          <li>
            <Link style={navStyle} to="/categories">
              Categories
            </Link>
          </li>
          <li>
            <Link style={navStyle} to="/search">
              Search
            </Link>
          </li>
          <li>
            <Link style={navStyle} to="/jokes">
              Jokes
            </Link>
          </li>
        </ul>
      </nav>
    );
  }
}

export default NavBar;
