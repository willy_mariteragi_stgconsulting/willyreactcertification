import React, { Component } from "react";

// See my comment in the `SubmitButton` component for how to apply props to 
// HTML elements that have lots of potential native attributes.

class InputField extends Component {
  render() {
    const {inputClassName,data_testid,disabled,...otherProps}= this.props;
    return (
        <input data-testid={ this.props.data_testid || "inputField-input"}
          className={ this.props.inputClassName || "input"}
          {...otherProps}
          onChange={(event) => this.props.onChange(event.target.value)}
        />
    );
  }
}

export default InputField;
