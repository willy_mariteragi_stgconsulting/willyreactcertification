import React from "react";

/*
  #functional component 
  
*/
const CustomPagination = ({ postsPerPage, totalPosts, paginate }) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i);
  }

  /*
  #Array Function - map
  map the array items into html code

  #Lists & Keys
  Key:React uses the key to match children in the original tree with children in the subsequent tree
  */
  const display = pageNumbers.map((number) => {
    return (
      <li key={number.toString()} className="page-item">
        <a
          data-testid={"custom-pagination-div-li-a-" + number}
          href="/search#"
          onClick={() => paginate(number)}
          className="page-link"
        >
          {number}
        </a>
      </li>
    );
  });

  return (
    <div data-testid="custom-pagination-div" className="custom-pagination">
      <nav className="custom-pagination">
        <ul className="pagination">{display}</ul>
      </nav>
    </div>
  );
};

export default CustomPagination;
