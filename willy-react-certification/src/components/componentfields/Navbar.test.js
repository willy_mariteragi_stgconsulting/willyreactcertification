import React from "react";
import { Link } from "react-router-dom";
import expect, { createSpy } from "expect";
import { shallow, mount, render } from "enzyme";
import NavBar from "./Navbar";

describe("Navigation Testing", () => {
  const wrapper = shallow(<NavBar />);
  const navStyle = {
    color: "white",
  };

  it("Should have nav Home", () => {
    const expectedElement = (
      <li>
        <Link style={navStyle} to="/">
          Home
        </Link>
      </li>
    );

    expect(wrapper.containsMatchingElement(expectedElement)).toBe(true);

    expect(wrapper.find("nav").length).toEqual(1);
  });

  it("Should have nav Categories", () => {
    const expectedElement = (
      <li>
        <Link style={navStyle} to="/categories">
          Categories
        </Link>
      </li>
    );

    expect(wrapper.containsMatchingElement(expectedElement)).toBe(true);
  });

  it("Should have nav Search", () => {
    const expectedElement = (
      <li>
        <Link style={navStyle} to="/search">
          Search
        </Link>
      </li>
    );

    expect(wrapper.containsMatchingElement(expectedElement)).toBe(true);
  });

  it("Should have nav Jokes", () => {
    const expectedElement = (
      <li>
        <Link style={navStyle} to="/jokes">
          Jokes
        </Link>
      </li>
    );

    expect(wrapper.containsMatchingElement(expectedElement)).toBe(true);
  });
});
