import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { screen, getByTestId } from "@testing-library/dom";
import { fireEvent } from "@testing-library/react";

import ModalDisplay from "./ModalDisplay";
import InputField from "./InputField";

let container = null;
const onRequestClose = jest.fn();
const title = "title";
const value = "value 123";
let testId = "";
let isOpen = false;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders ModalDisplay. Modal display is closed", () => {
  isOpen = false;
  testId = "modaldisplay-div-is-not-open";
  act(() => {
    render(
      <ModalDisplay
        onRequestClose={onRequestClose}
        isOpen={isOpen}
        title={title}
        value={value}
      />,
      container
    );
  });

  expect(screen.getByTestId(testId)).toBeInTheDocument();
});

it("onRequestClose called", () => {
  isOpen = true;
  testId = "modaldisplay-modal-close-button";
  act(() => {
    render(
      <ModalDisplay
        onRequestClose={onRequestClose}
        isOpen={isOpen}
        title={title}
        value={value}
      />,
      container
    );
  });

  const closeBbutton = screen.getByTestId(testId);
  fireEvent(
    closeBbutton,
    new MouseEvent("click", {
      bubbles: true,
      cancelable: true,
    })
  );

  expect(closeBbutton).toBeInTheDocument();
  expect(onRequestClose).toHaveBeenCalledTimes(1);
});

it("renders ModalDisplay. Modal display is open", () => {
  isOpen = true;
  testId = "modaldisplay-div-is-open";
  act(() => {
    render(
      <ModalDisplay
        onRequestClose={onRequestClose}
        isOpen={isOpen}
        title={title}
        value={value}
      />,
      container
    );
  });

  expect(screen.getByTestId(testId)).toBeInTheDocument();
});
