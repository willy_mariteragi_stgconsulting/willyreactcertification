import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { screen, getByTestId } from "@testing-library/dom";
import { fireEvent } from "@testing-library/react";

import InputField from "./InputField";

let container = null;
let inputClassName = null;
let type = null;
let placeholder = null;
let value = null;
let onChange = null;
jest.useFakeTimers();
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);

  inputClassName = "search-input";
  type = "text";
  placeholder = "Search...";
  value = "value 123";

  onChange = jest.fn();
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders InputField", () => {
  const onChange1 = jest.fn();
  act(() => {
    render(
      <InputField
        inputClassName={inputClassName}
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={onChange1}
      />,
      container
    );
  });

  expect(container.firstChild).toHaveClass(inputClassName);

  fireEvent.click(screen.getByTestId("inputField-input"));
});
