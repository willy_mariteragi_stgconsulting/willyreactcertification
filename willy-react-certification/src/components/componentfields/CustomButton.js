import React, { Component } from "react";

class CustomButton extends Component {
  
  render() {
  const {buttonClassName,data_testid,disabled,...otherProps}= this.props;
    return (
        <button
          className={buttonClassName || "app-btn"}
          data-testid={data_testid || ""}
          disabled={disabled}
          {...otherProps}
          onClick={() => this.props.onClick()}
        >
          {this.props.text}
        </button>
    );
  }
}

export default CustomButton;
