import React from "react";
import { fireEvent } from "@testing-library/react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { screen } from "@testing-library/dom";

import CustomPagination from "./CustomPagination";

let container = null;
const pageNumbers = [];
const postsPerPage = 10;
const totalPosts = 50;
const paginate = () => {};

beforeEach(() => {
  
  container = document.createElement("div");
  document.body.appendChild(container);

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i);
  }
});

afterEach(() => {
  
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("renders PageNumber", () => {
  act(() => {
    render(
      <CustomPagination
        postsPerPage={postsPerPage}
        totalPosts={totalPosts}
        paginate={paginate}
      />,
      container
    );
  });
  
  pageNumbers.forEach((item) => {
    expect(container.textContent).toContain(item);
  });
});

it("Click on Paginate", () => {
  act(() => {
    render(
      <CustomPagination
        postsPerPage={postsPerPage}
        totalPosts={totalPosts}
        paginate={paginate}
      />,
      container
    );
  });
  const testId = "custom-pagination-div-li-a-1";
  const button = screen.getByTestId(testId);
  fireEvent(
    button,
    new MouseEvent("click", {
      bubbles: true,
      cancelable: true,
    })
  );
  
});
