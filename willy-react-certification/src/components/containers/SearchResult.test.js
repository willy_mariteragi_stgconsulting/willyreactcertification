import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { screen } from "@testing-library/dom";
import { fireEvent } from "@testing-library/react";
import SearchResult from "./SearchResult";
import store from "../../redux/store/Store";
import { Provider } from "react-redux";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

let currentPosts = [
  {
    categories: ["career"],
    id: "zk14uc6xr82d7ig9qhaymg",
    value:
      "Chuck Norris is actually the front man for Apple. He let's Steve Jobs run the show when he's on a mission. Chuck Norris is always on a mission.",
    jokeId: 1,
  },
  {
    categories: ["celebrity"],
    id: "t14_olJKRAyw8NkK6rlr1g",
    value: "Charlie Sheen is Chuck Norris' lovechild.",
    jokeId: 2,
  },
];

const loading = false;
const paginate = () => {};
const onRequestClose = () => {};
let isOpen = false;

describe("SearchResult Testing", () => {
  it("search result value are showing", () => {
    const testId = "searchresult-ul-li-al-";
    act(() => {
      render(
        <Provider store={store}>
          <SearchResult
            posts={currentPosts}
            loading={loading}
            paginate={paginate}
            onRequestClose={onRequestClose}
            isOpen={isOpen}
          />
        </Provider>,
        container
      );
    });
    let tempId = 0;
    currentPosts.forEach((item) => {
     let  tempTestId = testId.concat(++tempId);

      const tempItem = item.value.substring(0,50);
      
      // console.log(tempItem);
      
      expect(container).toHaveTextContent(tempItem);
      // const temp = screen.getByText(tempItem);
      const temp = screen.getByTestId(tempTestId);

      expect(temp).toHaveAttribute("data-toggle", "modal");
    });
  });

  it("search result value are showing", () => {
    const testId = "searchresult-ul";

    act(() => {
      render(
        <Provider store={store}>
          <SearchResult
            posts={currentPosts}
            loading={loading}
            paginate={paginate}
            onRequestClose={onRequestClose}
            isOpen={isOpen}
          />
        </Provider>,
        container
      );
    });

    const temp = screen.getByTestId("searchresult-ul-li-al-1");

    fireEvent(
      temp,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );
    const modalDisplay = screen.getByTestId("modaldisplay-div-is-open");

    expect(modalDisplay).toBeInTheDocument();
  });

  it("attemp to Click ", () => {
    act(() => {
      render(
        <Provider store={store}>
          <SearchResult
            posts={currentPosts}
            loading={loading}
            paginate={paginate}
            onRequestClose={onRequestClose}
            isOpen={isOpen}
          />
        </Provider>,
        container
      );
    });

    const tempSearchResult = screen.getByTestId("searchresult-ul-li-al-1");
    fireEvent(
      tempSearchResult,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    const temp = screen.getByTestId("modaldisplay-div-is-open");
    fireEvent(
      temp,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    const modalDisplay = screen.getByTestId("modaldisplay-div-is-open");

    expect(modalDisplay).toBeInTheDocument();
  });
});
