import React from "react";
import useSortTableData from "../../helpers/UseSortTableData";

/*
  #custom Hook
  #React Hooks
   we feed it a list. in return, we get 2 object and 1 function
   items: contains the sorted list
   requestSort: function to set what to sort it by
   sortConfig: to update the class name to show ascending or descending
 */

const JokeTable = (props) => {
  /*
  #Closure 
  useSortTableData because i am accessing a variable defined in a function scope even after the function exited is known as closure
  
  #Destructuring
   assigning values from a function
   */
  const { items, requestSort, sortConfig } = useSortTableData(props.jokes);
  const getClassNamesFor = (name) => {
    if (!sortConfig) {
      return;
    }

    return sortConfig.key === name ? sortConfig.direction : undefined;
  };
  let temp = [];

  if (items.length > 0) {
    temp = items.map((item) => {
      return (
        <tr key={item.jokeId}>
          <td>{item.jokeId}</td>
          <td>{item.categories.map((category) => category)}</td>
          <td>{item.value}</td>
        </tr>
      );
    });
  }

  return (
    <table>
      <thead>
        <tr>
          <th>
            <button
              data-testid="joketable-sortby-jokeid"
              type="button"
              onClick={() => requestSort("jokeId")}
              className={getClassNamesFor("jokeId")}
            >
              Id
            </button>
          </th>
          <th>
            <button
              type="button"
              data-testid="joketable-sortby-categories"
              onClick={() => requestSort("categories")}
              className={getClassNamesFor("categories")}
            >
              categories
            </button>
          </th>
          <th>
            <button
              type="button"
              data-testid="joketable-sortby-value"
              onClick={() => requestSort("value")}
              className={getClassNamesFor("value")}
            >
              jokes
            </button>
          </th>
        </tr>
      </thead>
      <tbody data-testid="joketable-tbody">{temp}</tbody>
    </table>
  );
};
export default JokeTable;
