import React, { Component } from "react";
import { Link } from "react-router-dom";

class Menu extends Component {
  render() {
    let linksMarkup = this.props.links.map((link) => {
      return (
        <li key={link.id} className="menu-link">
          <Link to={{pathname:link.link}}>{link.label}</Link>
          <p>{link.description}</p>
        </li>
      );
    });
    return (
      <div>
        <menu>{linksMarkup}</menu>
      </div>
    );
  }
}

export default Menu;
