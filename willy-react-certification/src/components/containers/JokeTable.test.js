import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { screen } from "@testing-library/dom";
import { fireEvent } from "@testing-library/react";

import JokeTable from "./JokeTable";

let container = null;
let jokes = [];
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);

  jokes = [
    {
      categories: ["career"],
      id: "zk14uc6xr82d7ig9qhaymg",
      value:
        "Chuck Norris is actually the front man for Apple. He let's Steve Jobs run the show when he's on a mission. Chuck Norris is always on a mission.",
      jokeId: 1,
    },
    {
      categories: ["celebrity"],
      id: "t14_olJKRAyw8NkK6rlr1g",
      value: "Charlie Sheen is Chuck Norris' lovechild.",
      jokeId: 2,
    },
    {
      categories: ["dev"],
      id: "41rkit60rnklc8w7fox1cq",
      value: "Chuck Norris can access the DB from the UI.",
      jokeId: 3,
    },
    {
      categories: ["fashion"],
      id: "0wdewlp2tz-mt_upesvrjw",
      value:
        "Chuck Norris does not follow fashion trends, they follow him. But then he turns around and kicks their ass. Nobody follows Chuck Norris.",
      jokeId: 4,
    },
    {
      categories: ["food"],
      id: "8danvsbpq_sa8uluveqn4a",
      value: "Chuck Norris ordered a Big Mac at Burger King, and got one.",
      jokeId: 5,
    },
  ];
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

function sortingAscending(items, key) {
  let tempSortedItems = [...items];

  tempSortedItems.sort((a, b) => {
    if (a[key] < b[key]) {
      return -1;
    }
    return 0;
  });
  return tempSortedItems;
}

function sortingDescending(items, key) {
  let tempSortedItems = [...items];

  tempSortedItems.sort((a, b) => {
    if (a[key] > b[key]) {
      return -1;
    }
    return 0;
  });
  return tempSortedItems;
}

describe("JokeTable Testing", () => {
  it("Joke Table with 1 joke", () => {
    act(() => {
      render(<JokeTable jokes={jokes} />, container);
    });

    jokes.forEach((item) => {
      expect(container).toHaveTextContent(item.jokeId);
      expect(container).toHaveTextContent(
        item.categories.map((category) => category)
      );
      expect(container).toHaveTextContent(item.value);
    });
  });

  it("table order by jokeId", () => {
    const testId = "joketable-tbody";
    act(() => {
      render(<JokeTable jokes={jokes} />, container);
    });

    const table = screen.getByTestId(testId);

    const sortedItem = sortingAscending(jokes, "jokeId");
    const button = screen.getByTestId("joketable-sortby-jokeid");
    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    let expected = "";
    for (let i = 0; i < sortedItem.length; i++) {
      expected =
        expected +
        `<tr><td>${sortedItem[i].jokeId}</td><td>${sortedItem[i].categories.map(
          (category) => category
        )}</td><td>${sortedItem[i].value}</td></tr>`;
    }

    expect(table.outerHTML).toContain(expected);
  });

  it("table order descending by jokeId", () => {
    const testId = "joketable-tbody";
    act(() => {
      render(<JokeTable jokes={jokes} />, container);
    });

    const table = screen.getByTestId(testId);
    const button = screen.getByTestId("joketable-sortby-jokeid");

    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );
    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    const sortedItem = sortingDescending(jokes, "jokeId");

    let expected = "";
    for (let i = 0; i < sortedItem.length; i++) {
      expected =
        expected +
        `<tr><td>${sortedItem[i].jokeId}</td><td>${sortedItem[i].categories.map(
          (category) => category
        )}</td><td>${sortedItem[i].value}</td></tr>`;
    }

    expect(table.outerHTML).toContain(expected);
  });

  it("table order by categories", () => {
    const testId = "joketable-tbody";
    act(() => {
      render(<JokeTable jokes={jokes} />, container);
    });

    const table = screen.getByTestId(testId);

    const sortedItem = sortingAscending(jokes, "categories");
    const button = screen.getByTestId("joketable-sortby-categories");
    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    let expected = "";
    for (let i = 0; i < sortedItem.length; i++) {
      expected =
        expected +
        `<tr><td>${sortedItem[i].jokeId}</td><td>${sortedItem[i].categories.map(
          (category) => category
        )}</td><td>${sortedItem[i].value}</td></tr>`;
    }

    expect(table.outerHTML).toContain(expected);
  });

  it("table order descending by categories", () => {
    const testId = "joketable-tbody";
    act(() => {
      render(<JokeTable jokes={jokes} />, container);
    });

    const table = screen.getByTestId(testId);
    const button = screen.getByTestId("joketable-sortby-categories");

    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );
    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    const sortedItem = sortingDescending(jokes, "categories");

    let expected = "";
    for (let i = 0; i < sortedItem.length; i++) {
      expected =
        expected +
        `<tr><td>${sortedItem[i].jokeId}</td><td>${sortedItem[i].categories.map(
          (category) => category
        )}</td><td>${sortedItem[i].value}</td></tr>`;
    }

    expect(table.outerHTML).toContain(expected);
  });

  it("table order by value", () => {
    const testId = "joketable-tbody";
    act(() => {
      render(<JokeTable jokes={jokes} />, container);
    });

    const table = screen.getByTestId(testId);

    const sortedItem = sortingAscending(jokes, "value");
    const button = screen.getByTestId("joketable-sortby-value");
    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    let expected = "";
    for (let i = 0; i < sortedItem.length; i++) {
      expected =
        expected +
        `<tr><td>${sortedItem[i].jokeId}</td><td>${sortedItem[i].categories.map(
          (category) => category
        )}</td><td>${sortedItem[i].value}</td></tr>`;
    }

    expect(table.outerHTML).toContain(expected);
  });

  it("table order descending by value", () => {
    const testId = "joketable-tbody";
    act(() => {
      render(<JokeTable jokes={jokes} />, container);
    });

    const table = screen.getByTestId(testId);
    const button = screen.getByTestId("joketable-sortby-value");

    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );
    fireEvent(
      button,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    const sortedItem = sortingDescending(jokes, "value");

    let expected = "";
    for (let i = 0; i < sortedItem.length; i++) {
      expected =
        expected +
        `<tr><td>${sortedItem[i].jokeId}</td><td>${sortedItem[i].categories.map(
          (category) => category
        )}</td><td>${sortedItem[i].value}</td></tr>`;
    }

    expect(table.outerHTML).toContain(expected);
  });
});
