import React, { Component } from "react";
import ModalDisplay from "../componentfields/ModalDisplay";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { addJoke } from "../../redux/actions/Index";


class SearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
      postTitle: "",
      postValue: "",
    };
  }

  SetModalValueOnClick(post) {
    this.setState({ postValue: post.value, postTitle: post.categories[0], show: true });
    this.props.addJoke(post)
  }

  onRequestClose = (e) => {
    this.setState((state) => ({
      show: !state.show,
    }));
  };
  render() {
    let tempId = 0;
    let tempResult;
    let truncateValue="";
    if (this.props.posts) {
      tempResult = this.props.posts.map((post) => {
        truncateValue = post.value.substring(0,50);
        return (
          <li key={post.id} className="list-group-item">
            <a
              data-testid={"searchresult-ul-li-al-".concat(++tempId)}
              data-toggle="modal"
              href="/search#"
              onClick={() =>
                this.SetModalValueOnClick(post)
              }
            >
              {truncateValue}
            </a>
          </li>
        );
      });
    }

    return (
      <div className="searchresult-div">
        <ModalDisplay
          onRequestClose={this.onRequestClose}
          isOpen={this.state.show}
          title={this.state.postTitle}
          value={this.state.postValue}
        />
        <ul
          data-testid="searchresult-ul"
          className="search-result list-group mb-4 search-list"
        >
          {tempResult}
        </ul>
      </div>
    );
  }
}
SearchResult.prototypes={
  addJoke: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
};

export default connect(null, { addJoke  })(SearchResult);

