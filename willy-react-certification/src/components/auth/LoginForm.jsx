import React, { Component } from "react";
import InputField from "../componentfields/InputField";
import CustomButton from "../componentfields/CustomButton";
import { connect } from "react-redux";
/*
#Lifecycle methods
 - constructor
  - Render
*/
class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      errors: [],
      isPasswordValid: false,
      isEmailValid:false,
      passwordInputFieldType: "password"
    };

    this.handleLoginClick = this.handleLoginClick.bind(this);
  }

  
  setInputValue(property, value) {
    value = value.trim();

    this.setState({
      [property]: value,
    });

    if (property === "email" && value.length > 0) {
      this.validateEmail(value);
    }

    if (property === "password"     ) {
      this.validatePassword(value);
    }
  }

  handleLoginClick() {
    let result = {
      data: {
        email: this.state.email,
        password: this.state.password,
      },
      bodyRequest: JSON.stringify({
        email: this.state.email,
        password: this.state.password,
      }),
    };
    this.props.handleLoginAuth(result);
  }

  toggleShowPassword() {
    this.setState((state)=>
    {
      return {
        ...state,
        passwordInputFieldType: state.passwordInputFieldType=== "password" ? "text":"password",
      }
    });
  }

  validateEmail(email) {
    const pattern = /[a-zA-Z0-9]+[.]?([a-zA-Z0-9]+)?[@][a-z]{3,9}[.][a-z]{2,3}/g;
  
    this.setState((state)=>{
      const result = pattern.test(email);

      const errorMessage = "Invalid email format";
      let error  = [...state.errors];
      if (result) {
         error = error.filter(e => e !== errorMessage);
      } else {
          this.addErrorIfNotExists(error, errorMessage);
      }

      return {
        ...state,
        isEmailValid: result,
        errors: error};
      });
  }

  validatePassword(password) {
    const oneCapitalRegex = /[A-Z]+/;
    const oneLowerRegex = /[a-z]+/;
    const oneDigitsRegex = /\d+/;

    this.setState((state)=>{

      const hasCapitalLetter = oneCapitalRegex.test(password);
      const hasLowerLetter = oneLowerRegex.test(password);
      const hasDigits = oneDigitsRegex.test(password);

      const sixToTen = password.length >= 6 && password.length <= 10;
      
      let errorMessage = [...state.errors];

      if (!hasCapitalLetter) {
        this.addErrorIfNotExists(errorMessage, "Upper case character is missing");
      } else {
        // errorMessage = errorMessage.filter(e=>e !== "Upper case character is missing");
        this.removeErrorIfExists(errorMessage, "Upper case character is missing");
      }
      if (!hasLowerLetter) {
        this.addErrorIfNotExists(errorMessage, "Lower case character is missing");
      } else {
        errorMessage = errorMessage.filter(e=>e !== "Lower case character is missing");
      }

      if (!hasDigits) {
        this.addErrorIfNotExists(errorMessage, "Digit character is missing");
      } else {
        errorMessage = errorMessage.filter(e=>e !== "Digit character is missing");
      }

      if (!sixToTen) {
        this.addErrorIfNotExists(errorMessage,"Password must have 6 to 10 characters");
      } else {
        errorMessage = errorMessage.filter(e=>e !== "Password must have 6 to 10 characters");
      }

      return {
        ...state,
        isPasswordValid: hasCapitalLetter && hasLowerLetter && hasDigits && sixToTen,
        errors: errorMessage
      }
    });
    
  }

  addErrorIfNotExists(myArray = [], message) {
    let test = this.state.errors.some((x) => {
      return x === message;
    });
    if (!test) {
      myArray.push(message);
    }
    return myArray;
  }

  removeErrorIfExists(myArray) {
    var what,
      a = arguments,
      L = a.length,
      ax;
    while (L > 1 && myArray.length) {
      what = a[--L];
      while ((ax = myArray.indexOf(what)) !== -1) {
        myArray.splice(ax, 1);
      }
    }
    return myArray;
  }

  render() {
    /*
    #Array Function - map
    map the array items into html code
  */
    let errors ;
    if(this.state.errors !== undefined &&  this.state.errors.length > 0){
      errors = this.state.errors.map(( item,index) => {
        return <p key={index}>{item}</p>;
      });
    }

    return (
      <div>
        <div className="login-form">
          Log In
          <InputField
            data_testid="login-form-inputfield-email"
            type="text"
            placeholder="Email"
            value={this.state.email ? this.state.email : ""}
            onChange={(value) => this.setInputValue("email", value)}
          />
          <div className="login-form-password_with-button">
            <InputField
              data_testid="login-form-inputfield-password"
              inputClassName="login-form-input-with-button"
              type={this.state.passwordInputFieldType}
              placeholder="Password"
              value={this.state.password ? this.state.password : ""}
              onChange={(value) => this.setInputValue("password", value)}
            />
            <CustomButton
              data_testid="login-form-submitbutton-showpassword"
              text={this.state.passwordInputFieldType=== "password" ? "Show Password" : "Hide Password" }
              buttonClassName="login-form-btn-show-hide-password"
              disabled={this.state.password.length  < 1 }
              onClick={() => this.toggleShowPassword()}
            />
          </div>
          <CustomButton
            data_testid="login-form-submitbutton-login"
            text="Login"
            disabled={(!this.state.isPasswordValid || !this.state.isEmailValid || (this.state.errors !== undefined &&  this.state.errors.length > 0))}
            onClick={(event) => this.handleLoginClick(event)}
          />
        </div>
        <div data_testid="login-form-submitbutton-error">{errors}</div>
      </div>
    );
  }
}

export default connect()(LoginForm);
