import React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";
import configureStore from "redux-mock-store";
import rootReducer from "../redux/reducers/Index";
import { screen } from "@testing-library/dom";
import Categories from "./Categories";
import store from "../redux/store/Store";
import thunk from "redux-thunk";
import { mockStoreData, mockItem, mockCategories } from "../testing/TestData";

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe("My Connected React-Redux Component", () => {
  let mockstore;
  let component;
  let item = mockItem;

  const getRandomByCategory = jest.fn(() => mockCategories);

  beforeEach(() => {
    mockstore = mockStore(mockStoreData);

    component = renderer.create(
      <Provider store={mockstore}>
        <Categories />
      </Provider>
    );
  });

  it("should render with given state from Redux store", () => {
    expect(component.toJSON()).toMatchSnapshot();
    
  });

  it("snapshot has categories", () => {
    mockCategories.forEach((item) => {
      expect(item).toMatchSnapshot();
    });
  });
});
