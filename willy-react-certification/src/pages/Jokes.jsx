import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import JokeTable from "../components/containers/JokeTable";

class Jokes extends Component {
  render() {
    if (!this.props.isLogged) {
      return (
        <div data-testid="redirect-to-home-page">
          <Redirect to="/" />
        </div>
      );
    } else {
      return (
        
        <React.Fragment>
          <h1>Jokes</h1>
          <JokeTable jokes={this.props.jokes} />
        </React.Fragment>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  isLogged: state.isLogged,
  jokes: state.categories.jokes,
});

export default connect(mapStateToProps)(Jokes);
