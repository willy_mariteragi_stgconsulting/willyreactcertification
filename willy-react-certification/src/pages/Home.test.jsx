import React from "react";
import { render, unmountComponentAtNode } from "react-dom";

import { Provider } from "react-redux";
import { act } from "react-dom/test-utils";
import { screen } from "@testing-library/dom";
import { fireEvent, waitFor } from "@testing-library/react";
import store from "../redux/store/Store";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";
import { login, logout } from "../redux/actions/Index";

import Home from "./Home";

let container = null;
let jokes = [];
const history = createMemoryHistory();
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

describe("Home Testing: login Page", () => {
  it("Login should be loaded", () => {
    store.dispatch(logout());
    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Home />
          </Router>
        </Provider>,
        container
      );
    });

    expect(container.textContent).toContain("Log In");
    expect(container.textContent).toContain("Show Password");
    expect(container.textContent).toContain("Login");
  });

  it("handleLoginAuth click, SPinner is Showing", () => {
    store.dispatch(logout());
    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Home />
          </Router>
        </Provider>,
        container
      );
    });

    const emailInputField = screen.getByTestId("login-form-inputfield-email");
    fireEvent.change(emailInputField, { target: { value: "test@test.co" } });
    fireEvent.keyDown(emailInputField, { key: "m", code: "Keym" });

    const passwordInputField = screen.getByTestId(
      "login-form-inputfield-password"
    );
    fireEvent.change(passwordInputField, { target: { value: "2wEfffF" } });
    fireEvent.change(passwordInputField, { target: { value: "2wEfffFe" } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });

    const loginButton = screen.getByTestId("login-form-submitbutton-login");
    fireEvent(
      loginButton,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    expect(container.textContent).toContain("Loading, please Wait...");
  });

  it("setTimeout called", async () => {
    jest.useFakeTimers();
    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Home />
          </Router>
        </Provider>,
        container
      );
    });

    const emailInputField = screen.getByTestId("login-form-inputfield-email");
    fireEvent.change(emailInputField, { target: { value: "test@test.co" } });
    fireEvent.keyDown(emailInputField, { key: "m", code: "Keym" });

    const passwordInputField = screen.getByTestId(
      "login-form-inputfield-password"
    );
    fireEvent.change(passwordInputField, { target: { value: "2wEfffF" } });
    fireEvent.change(passwordInputField, { target: { value: "2wEfffFe" } });
    fireEvent.keyDown(passwordInputField, { key: "m", code: "Keym" });

    const loginButton = screen.getByTestId("login-form-submitbutton-login");

    fireEvent(
      loginButton,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    expect(setTimeout).toHaveBeenCalledTimes(2);
    expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 1500);

    jest.runOnlyPendingTimers();
    expect(setTimeout).toHaveBeenCalledTimes(3);
  });
});

describe("Home Testing: Main Page", () => {
  it("user is logged in, main Page Loaded", () => {
    store.dispatch(login(true));
    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Home />
          </Router>
        </Provider>,
        container
      );
    });

    expect(container.textContent).toContain("Welcome Home");

    expect(container.textContent).toContain("Home");
    expect(container.textContent).toContain(
      "The main (landing) page && login page"
    );

    expect(container.textContent).toContain("Categories");
    expect(container.textContent).toContain(
      "A page which will contain all joke categories"
    );

    expect(container.textContent).toContain("Search");
    expect(container.textContent).toContain(
      "A page to search for a joke by a search term"
    );

    expect(container.textContent).toContain("Jokes");
    expect(container.textContent).toContain(
      "A page the lists all viewed jokes"
    );
  });

  it("main Page Loaded, log out", () => {
    store.dispatch(login(true));
    act(() => {
      render(
        <Provider store={store}>
          <Router history={history}>
            <Home />
          </Router>
        </Provider>,
        container
      );
    });

    const logoutButton = screen.getByTestId("home-logout-button");

    fireEvent(
      logoutButton,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
      })
    );

    expect(container.textContent).toContain("Log In");
    expect(container.textContent).toContain("Show Password");
    expect(container.textContent).toContain("Login");
  });
});
