import { getCategories } from "./Category.Actions";

import { mockCategories, mockItem } from "../../testing/TestData";
import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import * as actions from "./category.actions";
import * as types from "../constants/ActionTypes";
import store from "../store/Store";
import configureMockStore from "redux-mock-store";
import fetchMock from "fetch-mock";

let mockFetch;
const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe("action testing", () => {
  it("fetchCategoriesSuccess", () => {
    const expectedActions = {
      type: types.FETCH_CATEGORIES_SUCCESS,
      payload: mockCategories,
    };

    expect(actions.fetchCategoriesSuccess(mockCategories)).toEqual(
      expectedActions
    );
  });

  it("fetchCategoriesFailed", () => {
    const ex = "THis is my ERROR";
    const expectedActions = {
      type: types.FETCH_CATEGORIES_FAILED,
      ex,
    };

    expect(actions.fetchCategoriesFailed(ex)).toEqual(expectedActions);
  });

  it("fetchRandomCategoriesSuccess", () => {
    const expectedActions = {
      type: types.FETCH_RANDOM_JOKE_BY_CATEGORY_SUCCESS,
      payload: mockCategories,
    };

    expect(actions.fetchRandomCategoriesSuccess(mockCategories)).toEqual(
      expectedActions
    );
  });

  it("fetchRandomCategoriesFailed", () => {
    const ex = "THis is my ERROR";
    const expectedActions = {
      type: types.FETCH_RANDOM_JOKE_BY_CATEGORY_FAILED,
      ex,
    };

    expect(actions.fetchRandomCategoriesFailed(ex)).toEqual(expectedActions);
  });

  it("showModal", () => {
    const payload = true;
    const expectedActions = {
      type: types.SHOWMODAL,
      payload,
    };

    expect(actions.showModal(payload)).toEqual(expectedActions);
  });
});

describe("Category.Actions Fetch Testing", () => {
  const middlewares = [thunk];
  const mockStore = configureMockStore(middlewares);

  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it("getCategories", () => {
    fetchMock.getOnce("https://api.chucknorris.io/jokes/categories", {
      body: mockCategories,
      headers: { "content-type": "application/json" },
    });

    const expectedActions = [
      { type: types.FETCH_CATEGORIES_SUCCESS, payload: mockCategories },
    ];
    const store = mockStore({ items: [] });

    return store.dispatch(actions.getCategories()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("getCategories ERROR", () => {
    fetchMock.mock("https://api.chucknorris.io/jokes/categories", 404);

    const expectedActions = [
      {
        type: types.FETCH_CATEGORIES_FAILED,
        ex:
          "category.actions.getCategories.error FetchError: invalid json response body at https://api.chucknorris.io/jokes/categories reason: Unexpected end of JSON input",
      },
    ];
    const store = mockStore({ items: [] });

    return store.dispatch(actions.getCategories()).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  it("getRandomByCategory ERROR - FETCH_RANDOM_JOKE_BY_CATEGORY_FAILED", () => {
    const category = "animal";
    fetchMock.mock(
      "https://api.chucknorris.io/jokes/random?category=" + category,
      404
    );

    const expectedActions = [
      {
        type: types.FETCH_RANDOM_JOKE_BY_CATEGORY_FAILED,
        ex: "",
      },
    ];

    const store = mockStore({ errors: [], items: [] });

    store.dispatch(actions.getRandomByCategory(category));
  });
});
