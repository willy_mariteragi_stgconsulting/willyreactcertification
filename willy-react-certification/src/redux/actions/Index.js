import * as actions from "../constants/ActionTypes";

export const login = (value) => {
  return { type: actions.SIGN_IN, payload: value };
};

export const logout = () => {
  return { type: actions.SIGN_OUT, payload: false };
};

export const addJoke = (value) => {
  return { type: actions.ADD_JOKE, payload: value };
};
