import * as actions from "../constants/ActionTypes";
/*
  #Redux Reducers
    simple Reducer to process user logging in / out
*/
const loggedReducer = (state = false, action) => {
  switch (action.type) {
    case actions.SIGN_IN:
      return action.payload;
    case actions.SIGN_OUT:
      return false;
    default:
      return state;
  }
};

export default loggedReducer;
