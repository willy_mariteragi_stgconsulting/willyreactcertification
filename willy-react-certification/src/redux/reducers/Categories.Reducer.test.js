import reducer from "./Categories.Reducer";
import * as types from "../constants/ActionTypes";

import { categories, mockResponse } from "../../testing/TestData";

describe("categories reducer", () => {
  let jokeId = 0;
  it("should return the initial state", () => {
    expect(reducer(undefined, {})).toEqual({
      items: [],
      item: {
        categories: [],
        value: "",
      },
      jokes: [],
      show: false,
      errors: [],
    });
  });

  it("should handle FETCH_CATEGORIES_SUCCESS", () => {
    expect(
      reducer([], {
        type: types.FETCH_CATEGORIES_SUCCESS,
        payload: categories,
      })
    ).toEqual({
      items: categories,
    });
  });

  it("should handle FETCH_CATEGORIES_FAILED", () => {
    const errorMessage = "categories Error";
    const expected = {
      errors: [errorMessage],
      items: [],
      item: {
        categories: [],
        value: "",
      },

      jokes: [],
      show: false,
    };
    expect(
      reducer(undefined, {
        type: types.FETCH_CATEGORIES_FAILED,
        payload: errorMessage,
      })
    ).toEqual(expected);
  });

  it("should handle FETCH_RANDOM_JOKE_BY_CATEGORY_SUCCESS", () => {
    jokeId = 1;//(mockResponse.result === undefined ? 0: mockResponse.result.length) + 1 ;

    const expected = {
      errors: [],
      items: [],
      item: mockResponse.result[0],
      jokes: [{ ...mockResponse.result[0], jokeId: jokeId}],
      show: true,
    };
    const actual = reducer(undefined, {
      type: types.FETCH_RANDOM_JOKE_BY_CATEGORY_SUCCESS,
      payload: mockResponse.result[0],
    });

    expect( actual).toEqual(expected);
  });

  it("should handle SHOWMODAL", () => {
    expect(
      reducer([], {
        type: types.SHOWMODAL,
        payload: true,
      })
    ).toEqual({
      show: true,
    });
  });

  it("should handle ADD_JOKE", () => {
    // console.log("should handle ADD_JOKE",mockResponse.result)
    jokeId = (mockResponse.result === undefined ? 0: mockResponse.result.length) + 1 ;

    const expected = {
      items: [],
      item: { categories: [], value: "" },
      jokes: [{ ...mockResponse.result[0], jokeId: 1 }],
      show: false,
      errors: [],
    };

    const actual = reducer(undefined, {
      type: types.ADD_JOKE,
      payload: mockResponse.result[0],
    });

    expect(actual).toEqual(expected);
  });
});
