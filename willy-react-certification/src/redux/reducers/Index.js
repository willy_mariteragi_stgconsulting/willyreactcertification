import { combineReducers } from "redux";

import loggedReducer from "./IsLogged";

import categoriesReducer from "./Categories.Reducer";

export default combineReducers({
  isLogged: loggedReducer,
  categories: categoriesReducer,
});
