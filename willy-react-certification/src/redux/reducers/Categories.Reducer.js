import * as actions from "../constants/ActionTypes";


const initialState = {
  errors: [],
  items: [],
  item: {
    categories: [],
    value: "",
  },
  jokes: [],
  show: false,
};

const categoriesReducer = (state = initialState, action) => {
  let  jokeId;
  switch (action.type) {
    case actions.FETCH_CATEGORIES_SUCCESS:
      return {
        ...state,
        items: action.payload,
      };

    case actions.FETCH_CATEGORIES_FAILED:
      return {
        ...state,
        errors: [...state.errors, action.payload],
      };

    case actions.FETCH_RANDOM_JOKE_BY_CATEGORY_SUCCESS:
      jokeId = (state.jokes === undefined ? 0: state.jokes.length) + 1 ;
      return {
        ...state,
        item: action.payload,
        jokes: [...state.jokes, { ...action.payload, jokeId: jokeId}],
        show: true,
      };
    case actions.SHOWMODAL:
      return {
        ...state,
        show: action.payload,
      };
    case actions.ADD_JOKE:
      
      jokeId = (state.jokes === undefined ? 0: state.jokes.length) + 1 ;
      return {
        ...state,
        jokes: [...state.jokes, { ...action.payload, jokeId: jokeId }],
      };

    default:
      return state;
  }
};

export default categoriesReducer;
