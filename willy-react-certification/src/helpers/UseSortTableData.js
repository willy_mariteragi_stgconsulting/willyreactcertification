import { useState, useMemo } from "react";

/*
 #React Hooks 2 listed below
 hooks are only used in function component to hook into react state and lifecycle. https://reactjs.org/docs/hooks-overview.html
  useState: used to set and get the sortConfig
  useMemo: this is used during rendering to update the sorting
  https://www.smashingmagazine.com/2020/03/sortable-tables-react/
*/
const useSortTableData = (items=[], config = { direction: "", key: "" }) => {
  const [sortConfig, setSortConfig] = useState(config);

  
  const sortedItems = useMemo(() => {
    let tempSortedItems = [...items];
    if (sortConfig !== null) {
      tempSortedItems.sort((a, b) => {
        if (a[sortConfig.key] < b[sortConfig.key]) {
          return sortConfig.direction === "ascending" ? -1 : 1;
        }
        if (a[sortConfig.key] > b[sortConfig.key]) {
          return sortConfig.direction === "ascending" ? 1 : -1;
        }
        return 0;
      });
    }
    return tempSortedItems;
  }, [items, sortConfig]);

  const requestSort = (key) => {
    let direction = "ascending";
    if (
      sortConfig &&
      sortConfig.key === key &&
      sortConfig.direction === "ascending"
    ) {
      direction = "descending";
    }

    setSortConfig({ key, direction });
  };

  return {
    items: sortedItems,
    requestSort,
    sortConfig,
  };
};

export default useSortTableData;
