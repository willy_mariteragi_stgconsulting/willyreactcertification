import React from "react";
import { unmountComponentAtNode } from "react-dom";
import { render, screen } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import store from "./redux/store/Store";
import { Provider } from "react-redux";
import App from "./App";
import { logout } from "./redux/actions";
describe("App", () => {
  it("renders Categories link", () => {
    store.dispatch(logout());

    const { getByText } = render(
      <Provider store={store}>
        <App />
      </Provider>
    );

    const categoryElement = getByText(/categories/i);

    expect(categoryElement).toBeInTheDocument();
  });

  it("renders search link", () => {
    store.dispatch(logout());

    const { getByText } = render(
      <Provider store={store}>
        <App />
      </Provider>
    );

    const searchElement = getByText(/search/i);

    expect(searchElement).toBeInTheDocument();
  });

  it("renders jokes link", () => {
    store.dispatch(logout());

    const { getByText } = render(
      <Provider store={store}>
        <App />
      </Provider>
    );

    const jokesElement = getByText(/jokes/i);

    expect(jokesElement).toBeInTheDocument();
  });
});
